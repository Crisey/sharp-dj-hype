﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Metro_sharpdj.Lua_component
{
    class SavingLuaContent
    {
        public string BaseColor;
        public string HeaderColor;
        public bool ConsoleSwitch;


        public string savingLuaContent() => "--[[ GLOBAL SETTINGS --]]" + Environment.NewLine +
        "BaseColor = \"" + BaseColor + "\"" + Environment.NewLine +
        "HeaderColor = \"" + HeaderColor + "\"" + Environment.NewLine +
        "Console = " + ConsoleSwitch.ToString().ToLower() +  Environment.NewLine +
        "--[[ MAIN MENU --]]" + Environment.NewLine +
        "--[[ ROOM --]]" + Environment.NewLine;

    }
}
