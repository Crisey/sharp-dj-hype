﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using MahApps.Metro;
using MahApps.Metro.Controls;
using Metro_sharpdj.Lua_component;
using NLua;

namespace Metro_sharpdj
{
    public class LuaLoadVariables
    {
        private Lua luaState;
        public string headerColor;//steel
        public string baseColor;//BaseDark
        public bool? ConsoleSwitch;
        private string path = Environment.CurrentDirectory + "\\scripts\\";
        private string fileName = "config.lua";

        private string sampleLuaContent =
            "--[[ GLOBAL SETTINGS --]]" + Environment.NewLine +
            "BaseColor = \"basedark\"" + Environment.NewLine +
            "HeaderColor = \"steel\"" + Environment.NewLine +
            "Console = false" + Environment.NewLine +
            "--[[ MAIN MENU --]]" + Environment.NewLine +
            "--[[ ROOM --]]" + Environment.NewLine;



        public void StartUp()
        {
            luaState = new Lua();
            LuaCreate(); 

            if (string.IsNullOrEmpty(headerColor))
                headerColor = "steel";
            if (string.IsNullOrEmpty(baseColor))
                baseColor = "basedark";
            if (!ConsoleSwitch.HasValue)
                ConsoleSwitch = false;
        }

        private void LuaCreate()
        {

            string source;

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                File.Create(path + fileName).Close();

            }

            if (!File.Exists(path + fileName))
                File.Create(path + fileName).Close();


            source = File.ReadAllText(path + fileName);
            if (string.IsNullOrEmpty(source))
                File.WriteAllText(path + fileName, sampleLuaContent);

            luaState.LoadCLRPackage();
            try
            {
                luaState.DoFile(path + fileName);
                baseColor = luaState["BaseColor"] as string;
                headerColor = luaState["HeaderColor"] as string;
                ConsoleSwitch = luaState["Console"] as bool?;
                

                Console.WriteLine("Base color: {0}", baseColor);
                Console.WriteLine("Header color: {0}", headerColor);
                Console.WriteLine("Console State: {0}", ConsoleSwitch.Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[LUA EXCEPTION]: " + ex.Message + "\n");
            }
        }

        public void LuaSaveVariable()
        {
            SavingLuaContent saving = new SavingLuaContent();
            saving.BaseColor = baseColor;
            saving.HeaderColor = headerColor;
            saving.ConsoleSwitch = ConsoleSwitch.Value;

            File.WriteAllText(path + fileName, saving.savingLuaContent());
        }
    }
}
