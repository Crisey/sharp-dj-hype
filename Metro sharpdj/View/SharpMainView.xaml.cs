﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro;
using MahApps.Metro.Controls;
using Metro_sharpdj.ViewModel;
using SharpEngine;

namespace Metro_sharpdj.View
{
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            Theme.Themes.ChangeAppStyle(Luas.luaLoad.baseColor, Luas.luaLoad.headerColor);

            this.DataContext = new SharpMainViewModel(this);

            InitializeComponent();

        }
    }
}
