﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Metro_sharpdj.ViewModel;

namespace Metro_sharpdj.View
{
    /// <summary>
    /// Interaction logic for SharpOptionsView.xaml
    /// </summary>
    public partial class SharpOptionsView : MetroWindow
    {
        public SharpOptionsView()
        {
            DataContext = new SharpOptionsViewModel();
            InitializeComponent();
        }
    }
}
