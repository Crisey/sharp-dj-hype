﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Metro_sharpdj.ViewModel
{
    internal class SharpOptionsViewModel : BaseViewModel
    {
        #region .ctor

        public SharpOptionsViewModel()
        {

        }

        #endregion .ctor

        #region Properties

        private string _themeColor;
        public string ThemeColor
        {
            get { return _themeColor; }
            set
            {
                _themeColor = value;
                OnPropertyChanged("ThemeColor");
            }
        }

        private string _baseColor;
        public string BaseColor
        {
            get { return _baseColor; }
            set
            {
                _baseColor = value;
                OnPropertyChanged("BaseColor");
            }
        }



        #endregion Properties

        #region Methods


        #endregion Methods

        #region Commands

        public ICommand ApplySettings
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (!string.IsNullOrEmpty(BaseColor))
                        Luas.luaLoad.baseColor = BaseColor;
                    if (!string.IsNullOrEmpty(ThemeColor))
                        Luas.luaLoad.headerColor = ThemeColor;
                    Luas.luaLoad.LuaSaveVariable();
                    Theme.Themes.ChangeAppStyle(BaseColor, ThemeColor);
                });
            }
        }

        #endregion Commands
    }
}
