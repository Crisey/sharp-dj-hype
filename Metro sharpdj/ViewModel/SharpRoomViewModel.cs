﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro_sharpdj.ViewModel
{
    class SharpRoomViewModel : BaseViewModel
    {
        #region .ctor

        public SharpRoomViewModel()
        {
            
        }

        #endregion .ctor

        #region Properties

        private string _chatText = "Crisey: Lol\nzonk256: Hahaha\nJeffDiggins: xoxoxoox";
        public string ChatText
        {
            get { return _chatText; }
            set
            {
                _chatText = value;
                OnPropertyChanged("ChatText");
            }
        }

        #endregion Properties

        #region Methods



        #endregion Methods

        #region Commands



        #endregion Commands
    }
}
