﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SharpEngine;

namespace Metro_sharpdj.ViewModel
{
    class SharpPluginManViewModel : BaseViewModel
    {
        #region imports



        #endregion imports

        #region .ctor

        public SharpPluginManViewModel()
        {

        }

        #endregion .ctor

        #region Properties

        private bool _pluginsSwitch = true;
        public bool PluginsSwitch
        {
            get { return _pluginsSwitch; }
            set
            {
                _pluginsSwitch = value;
                OnPropertyChanged("PluginsSwitch");
            }
        }

        private bool _consoleSwitch = Luas.luaLoad.ConsoleSwitch.Value;
        public bool ConsoleSwitch
        {
            get { return _consoleSwitch; }
            set
            {
                _consoleSwitch = value;
                OnPropertyChanged("ConsoleSwitch");

                if (_consoleSwitch)
                {
                    ConsoleManager.SetVisible(ConsoleManager.SW_SHOW);
                    Luas.luaLoad.ConsoleSwitch = true;
                }
                else
                {
                    ConsoleManager.SetVisible(ConsoleManager.SW_HIDE);
                    Luas.luaLoad.ConsoleSwitch = false;
                }
                Luas.luaLoad.LuaSaveVariable();


            }
        }

        private bool _logsSwitch = true;
        public bool LogsSwitch
        {
            get { return _logsSwitch; }
            set
            {
                _logsSwitch = value;
                OnPropertyChanged("LogsSwitch");
            }
        }



        #endregion Properties

        #region Methods


        #endregion Methods

        #region Commands
        //TODO: CopyLogs, ClearLogs

        public ICommand ClearLogs
        {
            get
            {
                return new RelayCommand(() =>
                {
                    //TODO: Clear Richtextbox
                }, () => true);
            }
        }

        public ICommand CopyLogs
        {
            get
            {
                return new RelayCommand(() =>
                {
                    //TODO: Copy Richtextbox to clipboard
                }, () => true);
            }
        }

        #endregion Commands
    }
}
