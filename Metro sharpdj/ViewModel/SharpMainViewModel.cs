﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using MahApps.Metro.Controls;
using Metro_sharpdj.View;

namespace Metro_sharpdj.ViewModel
{
    class SharpMainViewModel : BaseViewModel
    {

        #region .ctor

        public SharpMainViewModel(MainWindow window)
        {

        }

        #endregion .ctor

        #region Fields

        private SharpOptionsView p_OptionsView;
        private SharpRoomView p_RoomView;
        private SharpMarketplaceView p_MarketplaceView = new SharpMarketplaceView();
        private SharpPluginManView p_PluginManagerView = new SharpPluginManView();

        #endregion Fields

        #region Properties
        private string _hideButtonIcon = "../Imgs/Lobby/ArrowLeft.png";
        public string HideButtonIcon
        {
            get { return _hideButtonIcon; }
            set
            {
                _hideButtonIcon = value;
                OnPropertyChanged("HideButtonIcon");
            }
        }


        private Visibility _homeVis = Visibility.Visible;
        public Visibility HomeVis
        {
            get { return _homeVis; }
            set
            {
                _homeVis = value;
                OnPropertyChanged("HomeVis");
            }
        }

        private Visibility _categoryVis = Visibility.Collapsed;
        public Visibility CategoryVis
        {
            get { return _categoryVis; }
            set
            {
                _categoryVis = value;
                OnPropertyChanged("CategoryVis");
            }
        }

        private Visibility _favoriteVis = Visibility.Collapsed;
        public Visibility FavoriteVis
        {
            get { return _favoriteVis; }
            set
            {
                _favoriteVis = value;
                OnPropertyChanged("FavoriteVis");
            }
        }

        private Visibility _mostPeopleVis = Visibility.Collapsed;
        public Visibility MostPeopleVis
        {
            get { return _mostPeopleVis; }
            set
            {
                _mostPeopleVis = value;
                OnPropertyChanged("MostPeopleVis");
            }
        }

        private Visibility _newsVis = Visibility.Collapsed;
        public Visibility NewsVis
        {
            get { return _newsVis; }
            set
            {
                _newsVis = value;
                OnPropertyChanged("NewsVis");
            }
        }


        private Visibility _leftBarVis;
        public Visibility LeftBarVis
        {
            get { return _leftBarVis; }
            set
            {
                _leftBarVis = value;
                OnPropertyChanged("LeftBarVis");
            }
        }

        private Visibility _pManVis;
        public Visibility PManVis
        {
            get { return _pManVis; }
            set
            {
                _pManVis = value;
                OnPropertyChanged("PManVis");
            }
        }
        #endregion Properties

        #region Methods

        public void SetVis(Visibility vis)
        {
            HomeVis = Visibility.Collapsed;
            FavoriteVis = Visibility.Collapsed;
            MostPeopleVis = Visibility.Collapsed;
            CategoryVis = Visibility.Collapsed;
            NewsVis = Visibility.Collapsed;
        }

        public void SetLeftBar()
        {
            string Hide = "../Imgs/Lobby/ArrowLeft.png";
            string Show = "../Imgs/Lobby/ArrowRight.png";

            if (LeftBarVis.Equals(Visibility.Collapsed))
            {
                LeftBarVis = Visibility.Visible;
                HideButtonIcon = Hide;
            }
            else
            {
                LeftBarVis = Visibility.Collapsed;
                HideButtonIcon = Show;
            }
        }

        #endregion Methods

        #region Commands

        public ICommand SetHome { get { return new RelayCommand(() => { SetVis(Visibility.Collapsed); HomeVis = Visibility.Visible; }, () => true); } }
        public ICommand SetFavorite { get { return new RelayCommand(() => { SetVis(Visibility.Collapsed); FavoriteVis = Visibility.Visible; }, () => true); } }
        public ICommand SetCategory { get { return new RelayCommand(() => { SetVis(Visibility.Collapsed); CategoryVis = Visibility.Visible; }, () => true); } }
        public ICommand SetMostPeople { get { return new RelayCommand(() => { SetVis(Visibility.Collapsed); MostPeopleVis = Visibility.Visible; }, () => true); } }
        public ICommand SetNews { get { return new RelayCommand(() => { SetVis(Visibility.Collapsed); NewsVis = Visibility.Visible; }, () => true); } }

        public ICommand OnScreenClick
        {
            get
            {
                return new RelayCommand(() =>
                {
                    p_RoomView = new SharpRoomView();
                    p_RoomView.ShowDialog();
                }, () => true);
            }
        }


        public ICommand HideLeftBar { get { return new RelayCommand(() => { SetLeftBar(); }); } }

        public ICommand OptionsBtn
        {
            get
            {
                return new RelayCommand(() =>
                {
                    p_OptionsView = new SharpOptionsView();
                    p_OptionsView.ShowDialog();
                }, () => true);
            }
        }

        public ICommand OpenMarketplace
        {
            get
            {
                return new RelayCommand(() =>
                {
                    p_MarketplaceView.Close();
                    p_MarketplaceView = new SharpMarketplaceView();
                    p_MarketplaceView.Show();
                }, () => true);
            }
        }

        public ICommand OpenPluginMan
        {
            get
            {
                return new RelayCommand(() =>
                {
                    p_PluginManagerView = new SharpPluginManView();
                    p_PluginManagerView.ShowDialog();
                }, () => true);
            }
        }
        #endregion Commands
    }
}
