﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using MahApps.Metro;
using NLua;
using SharpEngine;

namespace Metro_sharpdj
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            
            Luas.luaLoad.StartUp();

            if (Convert.ToBoolean(Luas.luaLoad.ConsoleSwitch))
                ConsoleManager.SetVisible(ConsoleManager.SW_SHOW);
            else
                ConsoleManager.SetVisible(ConsoleManager.SW_HIDE);

        }
    }
}
