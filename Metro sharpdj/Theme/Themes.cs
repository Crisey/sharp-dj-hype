﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro;
using MahApps.Metro.Controls;

namespace Metro_sharpdj.Theme
{
    public static class Themes
    {
        /*                    private static LuaLoadVariables luaLoad;

        luaLoad = new LuaLoadVariables();
            luaLoad.StartUp();*/

        public static void ChangeAppStyle(string baseColor, string themeColor)
        {
            var theme = ThemeManager.DetectAppStyle(Application.Current);

            if (string.IsNullOrEmpty(baseColor))
                baseColor = Luas.luaLoad.baseColor;
            if (string.IsNullOrEmpty(themeColor))
                themeColor = Luas.luaLoad.headerColor;

            ThemeManager.ChangeAppStyle(Application.Current,
                                        ThemeManager.GetAccent(themeColor),
                                        ThemeManager.GetAppTheme(baseColor));
        }
    }
}
