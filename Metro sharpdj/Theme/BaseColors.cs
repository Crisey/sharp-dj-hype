﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro_sharpdj.Theme
{
    public class BaseColors : ObservableCollection<string>
    {
        public BaseColors()
        {
            Add("basedark");
            Add("baselight");
        }
    }
}
