﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro_sharpdj.Theme
{
    public class ThemeColors : ObservableCollection<string>
    {
        public ThemeColors()
        {
            Add("amber");
            Add("blue");
            Add("brown");
            Add("cobalt");
            Add("crimson");
            Add("cyan");
            Add("emerald");
            Add("green");
            Add("indigo");
            Add("lime");
            Add("magenta");
            Add("mauve");
            Add("olive");
            Add("orange");
            Add("pink");
            Add("purple");
            Add("red");
            Add("sienna");
            Add("steel");
            Add("taupe");
            Add("teal");
            Add("violet");
            Add("yellow");
        }
    }
}
