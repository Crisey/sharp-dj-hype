﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SharpEngine.IO
{
    public class OwnFiles
    {
        public string[] GetPlugins(string extensionName = "SDJ")
        {
            string currentPath = Environment.CurrentDirectory + @"\Plugins\";
            string[] Files = Directory.GetFiles(currentPath, "*"+ extensionName +".*");

            return Files;
        }



    }
}
